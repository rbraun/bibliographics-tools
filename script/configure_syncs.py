# This script iterates all of the sqs queues and configures ones related to bibliographic syncs
# with the appropriate lambda handler for that queue.
from client.aws.aws_sqs import SqsClient
from client.aws.aws_lambda import LambdaClient
from client.database.postgres import ApplicationDatabase, get_client_config
from environment import Environment
from enum import Enum
from collections import defaultdict

# this can be set down to an individual queue if you just need to tweak its configuration alone
BIBLIOGRAPHIC_SYNC_PREFIX = 'bibliographics-sync-identifier-'

MARC_BIB_QUEUE_NAME = 'bibliographics-sync-marc-bib-{client_id}'
MARC_AUTH_QUEUE_NAME = 'bibliographics-sync-marc-auth-{client_id}'
MARC_CORRECTION_QUEUE_NAME = 'bibliographics-sync-marc-correction-{client_id}'

# these client ids are known not to exist (or have config problems) in the given environment
EXCLUSIONS = {
    Environment.STAGE: [1, 78, 106, 125, 136, 154],
    Environment.LIVE: [
        15,  # never used
        22,  # never used
        28,  # never used
        42,  # library DB was deleted
        52,  # we don't support sierra with authorities (III_ORACLE) anymore
        64,  # aliased to 52
        65  # aliased to 52
    ]
}

# if true, marc lambdas will be set up with triggers even if their ils syncs are disabled.
# you might do this if a stage or retired library needs its marc reprocessed, but generally
# we don't want triggers on queues that are inactive due to incurred expense.
OVERRIDE_SYNC_DISABLED = True


class UnhandledQueueException(Exception):
    pass


class MarcFlavour(Enum):
    def __init__(self, indicators):
        self.indicators = indicators

    CARLX = ['CARLX', 'CARL']
    EVERGREEN = ['EVERGREEN', 'VTLS', 'VTLS_Legacy', 'DESTINY']
    HORIZON = ['HORIZON_3.0']
    POLARIS = ['POLARIS']
    SIERRA = ['III']
    SYMPHONY = ['UNICORN_EMB_ORACLE', 'UNICORNAPI', 'UNICORN']

    @staticmethod
    def lookup_lowercased(indicator):
        for ils_type in MarcFlavour:
            if indicator in ils_type.indicators:
                return str(ils_type.name).lower()
        raise Exception(f"Unknown ils indicator {indicator}.")


class QueuePrefix(Enum):
    DIGITAL = 'bibliographics-sync-digital-'
    ENHANCEMENT = 'bibliographics-sync-enhancement-'
    MARC_BIB = 'bibliographics-sync-marc-bib-'
    MARC_AUTH = 'bibliographics-sync-marc-auth-'
    MARC_CORRECTION = 'bibliographics-sync-marc-correction-'
    IDENTIFIER = 'bibliographics-sync-identifier-'
    DLQ = "bibliographics-sync-dlq"

    def is_prefix_of(self, queue_name):
        return queue_name.startswith(self.value)

    def strip_prefix(self, queue_name):
        return queue_name.removeprefix(self.value)


def _configure_bibliographic_sync_queues(sqs_client, lambda_client):
    queue_lambda_map = _map_queues_to_lambdas(sqs_client)

    lambda_queues_map = defaultdict(list)
    for key, value in queue_lambda_map.items():
        lambda_queues_map[value].append(key)

    lambda_queue_change_map = _diff_all_lambda_sqs_triggers(lambda_client, lambda_queues_map)
    any_changes_required = _describe_configuration_changes(lambda_queue_change_map)

    if any_changes_required:
        print("Type yes to make these changes> ")
        choice = input().lower()
        if choice == 'yes':
            _apply_configuration_changes(lambda_client, lambda_queue_change_map)
            print("Changes applied.")
        else:
            print("No changes were made.")
    else:
        print("No changes are required, configuration appears to be up-to-date.")


def _describe_configuration_changes(lambda_queue_change_map):
    print("\n*********************")
    print("** CHANGE MANIFEST **")
    print("*********************\n")

    any_changes_required = False

    for function_name, update_tuple in lambda_queue_change_map.items():
        if update_tuple[0] or update_tuple[1]:
            any_changes_required = True
            print(f"{function_name} requires the following trigger changes:")
            for queue_arn in update_tuple[0]:
                print(f"  + {queue_arn}")
            for queue_arn in update_tuple[1]:
                print(f"  - {queue_arn}")
        else:
            print(f"{function_name} requires no changes.")

    return any_changes_required


def _apply_configuration_changes(lambda_client, lambda_queue_change_map):
    for function_name, update_tuple in lambda_queue_change_map.items():
        for queue_arn in update_tuple[1]:
            lambda_client.delete_lambda_sqs_trigger(function_name, queue_arn)
        for queue_arn in update_tuple[0]:
            lambda_client.add_lambda_sqs_trigger(function_name, queue_arn)


def _map_queues_to_lambdas(sqs_client):
    queue_lambda_map = {}
    for queue in sqs_client.get_sqs_queues_by_prefix(BIBLIOGRAPHIC_SYNC_PREFIX):
        try:
            function_name = _determine_function_name(queue)
            if function_name:
                queue_lambda_map[queue] = function_name
        except UnhandledQueueException as uqe:
            print(uqe)

    return queue_lambda_map


def _determine_function_name(queue):
    if QueuePrefix.ENHANCEMENT.is_prefix_of(queue.name):
        return queue.name
    if QueuePrefix.DIGITAL.is_prefix_of(queue.name):
        return queue.name
    if QueuePrefix.MARC_AUTH.is_prefix_of(queue.name):
        return _determine_marc_auth_function_name(queue)
    if QueuePrefix.MARC_BIB.is_prefix_of(queue.name):
        return _determine_marc_bib_function_name(queue)
    if QueuePrefix.MARC_CORRECTION.is_prefix_of(queue.name):
        return _determine_marc_correction_function_name(queue)
    if QueuePrefix.IDENTIFIER.is_prefix_of(queue.name):
        return queue.name
    if QueuePrefix.DLQ.is_prefix_of(queue.name):
        return None

    raise UnhandledQueueException(f"{queue.name} is not a supported queue name.")


def _determine_marc_auth_function_name(queue):
    try:
        source_id = int(QueuePrefix.MARC_AUTH.strip_prefix(queue.name))
        if source_id in EXCLUSIONS[environment]:
            return None

        agency_code = app_db.get_agency_code(source_id)

        source_details = get_client_config(environment, agency_code)
        lambda_suffix = MarcFlavour.lookup_lowercased(source_details['ils']) + \
                        (" (RETIRED)" if source_details['retired'] else "")

        if source_details['authorities']:
            if source_details['ils_enabled']:
                return QueuePrefix.MARC_AUTH.value + lambda_suffix
            elif OVERRIDE_SYNC_DISABLED:
                print(f"{queue.name} is {lambda_suffix} and uses auths but sync is off (overriding).")
                return QueuePrefix.MARC_AUTH.value + lambda_suffix
            else:
                print(f"{queue.name} is {lambda_suffix} and uses auths but sync is off (skipping).")
                return None
        else:
            print(f"{queue.name} is {lambda_suffix} but does not use auths.")
            return None
    except Exception as e:
        raise UnhandledQueueException(f"{queue.name} could not be processed: {str(e)}")


def _determine_marc_bib_function_name(queue):
    try:
        source_id = int(QueuePrefix.MARC_BIB.strip_prefix(queue.name))
        if source_id in EXCLUSIONS[environment]:
            return None

        agency_code = app_db.get_agency_code(source_id)

        source_details = get_client_config(environment, agency_code)
        lambda_suffix = MarcFlavour.lookup_lowercased(source_details['ils']) + \
                        (" (RETIRED)" if source_details['retired'] else "")

        if source_details['ils_enabled']:
            return QueuePrefix.MARC_BIB.value + lambda_suffix
        elif OVERRIDE_SYNC_DISABLED:
            print(f"{queue.name} is {lambda_suffix} but sync is off (overriding).")
            return QueuePrefix.MARC_BIB.value + lambda_suffix
        else:
            print(f"{queue.name} is {lambda_suffix} but sync is off (skipping).")
            return None
    except Exception as e:
        raise UnhandledQueueException(f"{queue.name} could not be processed: {str(e)}")


def _determine_marc_correction_function_name(queue):
    try:
        source_id = int(QueuePrefix.MARC_CORRECTION.strip_prefix(queue.name))
        if source_id in EXCLUSIONS[environment]:
            return None

        agency_code = app_db.get_agency_code(source_id)
        source_details = get_client_config(environment, agency_code)

        qualifier = " (RETIRED)" if source_details['retired'] else ""

        if source_details['correction_enabled']:
            return QueuePrefix.MARC_CORRECTION.value.removesuffix("-")
        elif OVERRIDE_SYNC_DISABLED:
            print(f"{queue.name}{qualifier} corrections is off (overriding).")
            return QueuePrefix.MARC_CORRECTION.value.removesuffix("-")
        else:
            print(f"{queue.name}{qualifier} corrections is off (skipping).")
            return None
    except Exception as e:
        raise UnhandledQueueException(f"{queue.name} could not be processed: {str(e)}")


def _diff_all_lambda_sqs_triggers(lambda_client, lambda_queues_map):
    # maps each lambda requiring update to a tuple of needed queue additions and deletions
    lambda_queue_change_map = {}

    for function_name, queues in lambda_queues_map.items():
        adds, deletes = _diff_sqs_triggers_for_lambda(lambda_client, function_name, queues)
        lambda_queue_change_map[function_name] = (adds, deletes)

    # add in any other known lambdas that may no longer have queues mapped to them
    for sync_lambda in lambda_client.list_functions(BIBLIOGRAPHIC_SYNC_PREFIX):
        if sync_lambda not in lambda_queues_map.keys():
            adds, deletes = _diff_sqs_triggers_for_lambda(lambda_client, sync_lambda, [])
            lambda_queue_change_map[sync_lambda] = (adds, deletes)

    return lambda_queue_change_map


def _diff_sqs_triggers_for_lambda(lambda_client, function_name, queues):
    existing_queue_arns = lambda_client.get_queue_arns_for_lambda(function_name)
    missing_queue_arns = []

    # any queue arns that don't match the scope of the current sync spec should be ignored
    for existing_queue_arn in list(existing_queue_arns):
        if BIBLIOGRAPHIC_SYNC_PREFIX not in existing_queue_arn:
            existing_queue_arns.remove(existing_queue_arn)

    for expected_queue_arn in list(queue.arn for queue in queues):
        if expected_queue_arn in existing_queue_arns:
            existing_queue_arns.remove(expected_queue_arn)
        else:
            missing_queue_arns.append(expected_queue_arn)

    # remaining arns in existing_queue_arns are unexpected
    return missing_queue_arns, existing_queue_arns


if __name__ == "__main__":
    environment = Environment.LIVE

    app_db = ApplicationDatabase(environment)
    sqs_client = SqsClient(environment)
    lambda_client = LambdaClient(environment)

    _configure_bibliographic_sync_queues(sqs_client, lambda_client)
