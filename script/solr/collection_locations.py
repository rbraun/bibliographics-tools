from environment import Environment
from client.solr.network import SolrNetwork

network = SolrNetwork(Environment.LIVE)
for collection, tuples in sorted(network.collections_map.items()):
    print(str(collection) + " -> " + str([host[0] for host in tuples]))
