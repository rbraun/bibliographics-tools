import csv

import docker as docker
import tabulate

from client.solr.status_client import ClusterStatusClient
from client.ssh.ssh_client import Ssh

COLLECTIONS = [
    'OR-WCCLS',
    'WA-KCLS',
    'CAL-SFPL'
]

CLIENT_HOST = 'solr-liv-cld21.bcommons.net'


class SpellcheckerAudit:
    HEADERS = ['collection', 'server', 'titleSuggester0']

    def __init__(self, hostname):
        status = ClusterStatusClient(hostname)
        self.collection_replicas = status.get_current_status().collection_replica_map
        self.table = []

    def audit_collection(self, collection):
        for replica in self.collection_replicas.get(collection):
            ssh = Ssh(replica.server)

            # look for file for one of the suggestion dictionaries
            res, error = ssh.execute_command('sudo docker inspect solrcloud | grep -Po ".*\K(/var.*_data)"')
            data = res.decode("utf-8").rstrip()
            awk = '\'{printf "%s %s %s %s", $5, $6, $7, $8}\''
            cmd = f'sudo ls -hl {data}/{replica.core}/data/titleSuggester0 | awk {awk}'
            res, error = ssh.execute_command(cmd)

            self.table.append([collection, replica.server, res.decode("utf-8").strip()])

    def print_audit(self):
        print(tabulate.tabulate(self.table, headers=self.HEADERS, tablefmt='fancy_grid'))

    def save(self):
        with open('spellcheckers.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self.HEADERS)
            writer.writeheader()

            for row in self.table:
                writer.writerow(dict(zip(self.HEADERS, row)))


if __name__ == '__main__':
    all_collections = ClusterStatusClient(CLIENT_HOST).get_current_status().get_catalog_collections()
    audit = SpellcheckerAudit(CLIENT_HOST)

    for collection in COLLECTIONS:
        audit.audit_collection(collection)
    audit.print_audit()
    #audit.save()
