from client.solr.collections_client import CollectionsClient
from client.solr.exceptions import ReplicaAlreadyExists

new_replicas = [
    'VI-RAPPAHANNOCK',
    'UGC-SHELF-VI-RAPPAHANNOCK',
]

receiver = 'solr-liv-cld31.bcommons.net'

node_client = CollectionsClient(receiver)

for replica in new_replicas:
    try:
        node_client.add_replica(replica)
        print()
    except ReplicaAlreadyExists:
        print("Skipping existing collection " + replica + ".")
