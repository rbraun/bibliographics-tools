from pprint import pprint
from time import sleep

from client.database.postgres import ApplicationDatabase
from client.solr.collections_client import CollectionsClient
from client.solr.status_client import ClusterStatusClient
from environment import Environment

SERVER_HOST = 'solr-liv-cld21.bcommons.net'

aliases = ClusterStatusClient(SERVER_HOST).get_current_status().get_aliases()
app_db = ApplicationDatabase(Environment.LIVE)
client = CollectionsClient(SERVER_HOST)

retired_sources = []
for alias, collection in aliases.items():
    if collection == 'DEV_NULL' and alias.startswith('core'):
        retired_sources.append(int(alias[4:]))

retired_collections = [app_db.get_agency_code(source) for source in sorted(retired_sources)]
for collection in retired_collections:
    shelf_collection = f'UGC-SHELF-{collection}'
    if shelf_collection in aliases:
        print(f'{shelf_collection} already exists, skipping...')
    else:
        client.create_alias(shelf_collection, 'DEV_NULL')
        sleep(1)
