from client.solr.collections_client import CollectionsClient
from client.solr.exceptions import ReplicaNotFound
from script.solr.check_recovery import wait_for_all_replicas_to_be_active

target_collections = [
    'VI-RAPPAHANNOCK',
    'UGC-SHELF-VI-RAPPAHANNOCK',
]

patient = 'solr-liv-cld32.bcommons.net'

node_client = CollectionsClient(patient)

for collection in target_collections:
    try:
        node_client.remove_replica(collection)
        print()
    except ReplicaNotFound:
        print("Collection " + collection + " does not exist on " + patient + ".")

print('Checking for recovering collections caused by leadership changes:')
wait_for_all_replicas_to_be_active(patient)
