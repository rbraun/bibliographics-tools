import time

from client.solr.status_client import ClusterStatusClient, Replica

CLIENT_HOST = 'solr-liv-cld21.bcommons.net'


def wait_for_all_replicas_to_be_active(host):
    status_client = ClusterStatusClient(host)
    time.sleep(5)
    while True:
        status = status_client.get_current_status()

        replica = status.is_any_replica_inactive()
        if not replica:
            print('All replicas are active!')
            break

        print(f'...{replica.collection} on {replica.server} is {replica.state}')
        time.sleep(30)


if __name__ == '__main__':
    wait_for_all_replicas_to_be_active(CLIENT_HOST)
