import json

import pandas as pd
import urllib.request

from client.solr.status_client import ClusterStatusClient
from environment import Environment
from client.solr.network import SolrNetwork

# TODO Move to solr client and generalize query capacity
UGC_BIB_QUERY = 'http://solr-liv-cld21.bcommons.net:8983/solr/UGC-BIB/select?indent=on&q=uc_lib_id:{lib_id}' \
                '%20uc_type:USER_CONTENT&wt=json'
CLIENT_HOST = 'solr-liv-cld21.bcommons.net'


def generate_allocations_csv(env: Environment.STAGE):
    network = SolrNetwork(env)

    dfs = []
    mems = []

    for node in network.solr_nodes:
        rows = [create_collection_row(collection, network, node) for collection in node.collections]
        rows.append([])

        combined_total = 0
        for row in rows:
            if len(row) == 8:
                combined_total += row[7]
        rows.append(['Total', node.total_size_gb, '', '', ''])

        mems.append([node.full_name, node.esx_host, node.total_size_gb])
        dfs.append([node.digit_name,
                    pd.DataFrame(rows, columns=['Collection', 'Size GB', 'Nodes', 'Esx Hosts', 'Notes'])])

    writer = pd.ExcelWriter(f'allocations_{env.value}.xlsx', engine='xlsxwriter')
    for df in dfs:
        df[1].to_excel(writer, sheet_name=df[0])

    mem_df = pd.DataFrame(data=mems, columns=['Node', 'ESX Host', 'Size GB'])
    mem_df.to_excel(writer, sheet_name="Totals")

    writer.close()


def get_collection_to_id_map():
    id_map = {}

    aliases = ClusterStatusClient(CLIENT_HOST).get_current_status().get_aliases()
    for k, v in aliases.items():
        if k.startswith('core'):
            id_map[v] = k[4:]

    return id_map


def create_collection_row(collection, network, node):
    node_ids = [node.digit_name]
    esx_ids = [node.esx_host]
    for host in network.collections_map[collection.name]:
        if host[0] != node.digit_name:
            node_ids.append(host[0])
            esx_ids.append(host[1])

    notes = ''
    if len(set(esx_ids)) != len(esx_ids):
        notes = "ERROR: ESX host duplication!"
        print(f"ERROR: {collection.name} on {node.short_name} is duplicated for ESX host {set(esx_ids)}!")

    return [collection.name, collection.size_gb, node_ids, esx_ids, notes]


def get_shelf_stats(collection, collection_id_map):
    if collection.name in collection_id_map:
        source_id = collection_id_map.get(collection.name)
        result = json.loads(urllib.request.urlopen(UGC_BIB_QUERY.format_map({'lib_id': source_id})).read())
        shelf_count = int(result['response']['numFound'])
        shelf_size = round(3.04353 * (shelf_count / 1000000) - 0.13445, 1)
        return shelf_count, shelf_size
    else:
        return 0, 0


if __name__ == "__main__":
    generate_allocations_csv(Environment.LIVE)
