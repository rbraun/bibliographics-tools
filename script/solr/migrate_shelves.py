from client.solr.collections_client import CollectionsClient
from client.solr.status_client import ClusterStatusClient
from client.ssh.ssh_client import Ssh
from script.solr.audit_shelf_counts import ShelfAudit

CLIENT_HOST = 'solr-liv-cld21.bcommons.net'

status_client = ClusterStatusClient(CLIENT_HOST)
ssh = Ssh(CLIENT_HOST)

status = status_client.get_current_status()


def _create_shelf_collections(collection):
    # just make sure collection is recognized
    catalog_replica_servers = [replica.server for replica in status.get_replicas_for_collection(collection)]
    if not any(catalog_replica_servers):
        raise Exception(f'No collection called {collection} exists.')

    # if replicas exist, locations must match collection locations; if so, we are done, otherwise error out
    shelf_collection = f'UGC-SHELF-{collection}'
    shelf_replicas = status.get_replicas_for_collection(shelf_collection)
    if any(shelf_replicas):
        if len(shelf_replicas) != len(catalog_replica_servers):
            raise Exception(f'Shelf count of {len(shelf_replicas)} does not match collection count '
                            f'of {len(catalog_replica_servers)} for {collection}.')
        for shelf_replica in shelf_replicas:
            if shelf_replica.server not in catalog_replica_servers:
                raise Exception(f'Replica {shelf_replica} is misplaced.')
        print(f'{shelf_collection} already exists, skipping it.')
        return

    # create the new shelf collection on the collection servers
    nodeset = ",".join((f'{server}:8983_solr' for server in catalog_replica_servers))
    node_client = CollectionsClient(CLIENT_HOST)
    node_client.create_collection(shelf_collection, nodeset)


def _create_backup_dir(collection):
    result, error = ssh.execute_command(f'sudo ls /solrbackups/UGC-SHELF-{collection}')
    if 'No such file or directory' in str(error):
        print(f'No backup dir for {collection}, creating it...')
        ssh.execute_command(f'sudo mkdir /solrbackups/UGC-SHELF-{collection}')
        result, error = ssh.execute_command(f'sudo ls /solrbackups/UGC-SHELF-{collection}')
        if 'No such file or directory' in str(error):
            raise Exception(f"ERROR: Couldn't create the backup directory for {collection}.")
        else:
            print(f"Successfully created backup dir for {collection}.")
    else:
        print(f'Backup dir for {collection} already exists.')


collections = status.get_catalog_collections()
errors = []
audit = ShelfAudit(CLIENT_HOST)
for collection in collections:
    try:
        _create_shelf_collections(collection)
        _create_backup_dir(collection)
        audit.audit_collection(collection)
    except Exception as e:
        errors.append(e)

print()
audit.print_audit()

print()
if errors:
    print("Fatal errors were reported:")
for error in errors:
    print(error)

