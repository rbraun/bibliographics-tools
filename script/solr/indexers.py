import time
from client.bitbucket.inventory import Inventory, ServerType
from client.ssh.ssh_client import Ssh
from environment import Environment

INDEXERS = [
    'slr-liv-smka-jsv02.bcommons.net',
    #'solr-liv-jsvindex08.bcommons.net',
    #'jobservicecloud003.core.prod.bf1.corp.pvt',
    #'jobservicecloud004.bcommons.net',
    #'jobservicecloud005.bcommons.net',
    #'solr-liv-jsvindex06.bcommons.net',
    #'solr-liv-jsvindex07.bcommons.net'
]


def _start(indexers):
    for indexer in indexers:
        ssh = Ssh(indexer)
        result, error = ssh.execute_command('sudo ps aux | grep JobService | grep -v grep')

        if not result:
            print(f'Starting indexer on {indexer}...')
            ssh.execute_command('sudo service jobservice start')
            while True:
                time.sleep(5)
                result, error = ssh.execute_command('sudo ps aux | grep JobService | grep -v grep')
                if result:
                    print('...started!')
                    break;
                print('...waiting for indexer to start...')

        else:
            print(f'Indexer is already running on {indexer}.')

        ssh.close()


def _stop(indexers):
    for indexer in indexers:
        ssh = Ssh(indexer)
        result, error = ssh.execute_command('sudo ps aux | grep JobService | grep -v grep')

        if result:
            print(f'Stopping indexer on {indexer}...')
            ssh.execute_command('sudo service jobservice stop')
            while True:
                time.sleep(5)
                result, error = ssh.execute_command('sudo ps aux | grep JobService | grep -v grep')
                if not result:
                    print('...stopped!')
                    break;
                print('...waiting for indexer to stop...')

        else:
            print(f'Indexer is not running on {indexer}.')

        ssh.close()


def _status(indexers):
    for indexer in indexers:
        ssh = Ssh(indexer)
        result, error = ssh.execute_command('sudo ps aux | grep JobService | grep -v grep')

        if result:
            print(f'Indexer is RUNNING on {indexer}.')
        else:
            print(f'Indexer is STOPPED on {indexer}.')

        ssh.close()


if __name__ == '__main__':
    all_indexers = Inventory(Environment.LIVE).hosts(ServerType.SOLR_INDEXERS)
    _stop(all_indexers)
    _start(all_indexers)
    _status(all_indexers)
