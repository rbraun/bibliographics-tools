import csv
from time import sleep

from client.solr.query_client import QueryClient
from client.solr.status_client import ClusterStatusClient
import tabulate

COLLECTIONS = [
    # 'ALB-CALGARY',
    # 'ALB-REDDEER',
    # 'ALB-STRATHCONA',
    # 'BC-FVRL',
    # 'BC-NEWWESTMINSTER',
    # 'BC-SURREY',
    # 'BC-WVML',
    # 'CAL-SCCL',
    # 'CAL-SFPL',
    # 'CAN-EPL',
    # 'CO-ARAPAHOE',
    # 'FL-BOCARATON',
    # 'IL-CPL-POLARIS',
    # 'KS-JOHNSONCOUNTY',
    # 'MA-BOSTON',
    # 'NB-OMAHA',
    # 'NC-CHAPELHILL',
    # 'NC-CHARLOTTEMECKLENBURG',
    # 'OH-WESTERVILLE',
    # 'OK-TULSA',
    # 'PA-EINETWORK',
    # 'TX-AUSTIN',
    # 'WA-KCLS',
    # 'WA-SEATTLE',
    # 'WA-SNOISLE',
    'WA-TACOMA'
]

SERVER_HOST = 'solr-liv-cld21.bcommons.net'


class ShelfAudit:
    HEADERS = ['collection', 'ugc-bib', 'shelf', 'diff', '%']

    def __init__(self, hostname):
        self.query = QueryClient(hostname)
        status = ClusterStatusClient(hostname)
        self.collection_sources = status.get_current_status().get_collection_source_ids()
        self.table = []

    def audit_collection(self, collection):
        source_id = self.collection_sources[collection]

        bib_count = self.query.get_result_count('UGC-BIB', f'uc_lib_id:{source_id} uc_type:USER_CONTENT')
        shelf_count = self.query.get_result_count(f'UGC-SHELF-{collection}', '*:*')

        self.table.append([collection, bib_count, shelf_count, bib_count - shelf_count,
                      round((shelf_count / bib_count) * 100, 4) if bib_count > 0 else (0 if shelf_count > 0 else 100)])
        sleep(1)

    def print_audit(self):
        print(tabulate.tabulate(self.table, headers=self.HEADERS, tablefmt='fancy_grid'))

    def save(self):
        with open('shelf_counts.csv', 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self.HEADERS)
            writer.writeheader()

            for row in self.table:
                writer.writerow(dict(zip(self.HEADERS, row)))


if __name__ == '__main__':
    audit = ShelfAudit(SERVER_HOST)
    status = ClusterStatusClient(SERVER_HOST)
    all_collections = status.get_current_status().get_catalog_collections()

    for collection in COLLECTIONS:
        audit.audit_collection(collection)
    audit.print_audit()
    audit.save()
