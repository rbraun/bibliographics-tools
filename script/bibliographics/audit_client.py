import psycopg2

from client.database import postgres
from environment import Environment
from logger import logging
import itertools
import client_aws
from client_aws import S3Folder
import sys

auth_only = False
aws = client_aws.AwsResources('live')


def sample(objects: set, count: int = 10000):
    return set(itertools.islice(objects, count))


def audit_client(environment, client):
    logging.info("*****************************************************************************************")

    config = postgres.get_client_config(environment, client)

    logging.info(f"Auditing {config['agency']}.")
    logging.info(f"Configuration = {config}")

    connection = psycopg2.connect(user="postgres",
                                  password='Xp16na@4',
                                  host=config["host"],
                                  port=config["port"],
                                  database=config["dbname"])

    if not config["authorities"]:
        logging.info(f"Client does not support authorities.")
    else:
        logging.info(f"Loading authority ids from DB...")

        cursor = connection.cursor()
        cursor.execute(f"select library_auth_id from marc_library_auth where lib_id = {config['id']} and tag = '000'")
        auth_ids = set(map(lambda x: f"S{config['id']}C" + str(x[0]), cursor.fetchall()))

        logging.info(f"{connection.dsn} = {len(auth_ids)}")

        client_aws.audit_folder(aws, config['id'], S3Folder.RAW_AUTHS, auth_ids)

        logging.info(f"Loading authority headings from DB...")

        cursor = connection.cursor()
        auth_query = f"select distinct on (library_auth_id) library_auth_id, tagdata from marc_library_auth " \
                     f"where lib_id = {config['id']} and tag ilike '1%' order by library_auth_id, marcauth_id" \
            if config['ils'] == 'POLARIS' \
            else f"select library_auth_id from marc_library_auth where lib_id = {config['id']} and tag ilike '1%'"
        cursor.execute(auth_query)

        # see bibliographics PolarisAuthUtils.getAuthLink for replacement rules
        auth_headings = set(map(lambda x: f"S{config['id']}C" + str(x[1]).replace(',', '')
                                .replace('.', '').replace('�', '_').replace(' ', "_"), cursor.fetchall())) \
            if config['ils'] == 'POLARIS' \
            else set(map(lambda x: f"S{config['id']}C{str(x[0])}", cursor.fetchall()))

        logging.info(f"{connection.dsn} = {len(auth_headings)}")

        client_aws.audit_folder(aws, config['id'], S3Folder.PROCESSED_AUTHS, auth_headings)

    if auth_only:
        exit(0)

    logging.info(f"Loading metadata ids from DB...")

    cursor = connection.cursor()
    cursor.execute(config["query"])
    db_ids = set(map(lambda x: f"S{config['id']}C" + str(x[0]), cursor.fetchall()))

    logging.info(f"{connection.dsn} = {len(db_ids)}")

    client_aws.audit_folder(aws, config['id'], S3Folder.RAW_BIBS, db_ids)
    if config['type'] == 'MARC':
        client_aws.audit_folder(aws, config['id'], S3Folder.PROCESSED_BIBS, db_ids)

    dynamodb_ids = client_aws.audit_dynamodb(aws, db_ids, config['type'])
    logging.info(f"Verified {len(dynamodb_ids)} metadata ids were present in dynamodb.")

    missing_dynamodb_ids = db_ids.difference(dynamodb_ids)
    if len(missing_dynamodb_ids) > 0:
        logging.error(
            f"{len(missing_dynamodb_ids)} metadata ids from DB missing from dynamodb: {sample(missing_dynamodb_ids)}")

    logging.info(f"Finished audit of {client}!")


if __name__ == "__main__":
    environment = Environment.LIVE
    clients = sys.argv[1].split(',')
    for client in clients:
        audit_client(environment, client)
