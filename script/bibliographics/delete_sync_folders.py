from environment import Environment
import enum
from client.aws.aws_s3 import S3Client

# This script was used to remove the various S3 sync buckets into which we used to save sync messages.
# Once we switched messages to use SQS instead of S3, this cleanup was needed.  Retaining the script
# in case more bulk S3 bucket cleanup is needed.


class SyncFolder(enum.Enum):
    AUTH_SYNC = 'auth_sync/'
    BIB_SYNC = 'bib_sync/'
    CONTENTCAFE_SYNC = 'contentcafe_sync/'
    IDREAMBOOKS_SYNC = 'idreambooks_sync/'
    SYNDETICS_SYNC = 'syndetics_sync/'
    BIB_CORRECTIONS = 'bib_corrections/'
    TMP_BIB_SYNC = 'tmp_bib_sync/'


if __name__ == "__main__":
    environment = Environment.STAGE

    s3_client = S3Client(environment)

    for bucket in s3_client.get_bucket_names_by_regex(f'bibservice-\d+-marc-records-{environment.value}'):
        print(f"Cleaning sync folders from {bucket}:")
        for folder in SyncFolder:
            print(f"   - Deleting objects from {folder.value} folder...", end="")
            count = s3_client.delete_folder_contents(bucket, folder.value)
            print(f"{count} deleted.")
