import re
import nltk
import json
import requests

# Download required NLTK packages (only need to do this once)
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')


def extract_contributions(text):
    # Define regular expressions to match contribution types
    contribution_regexes = [
        r'\b(author|writer)\b',
        r'\b(editor|compiler)\b',
        r'\b(translator)\b',
        r'\b(illustrator)\b'
    ]

    # Split the MARC record into subfields

    # Extract person names and associated contribution types
    contributions = set()

    # Skip any subfields that don't contain text
    if len(text) == 0:
        return []

    # Use NLTK's named-entity tagger to identify named entities in the text
    tagged_tokens = nltk.pos_tag(nltk.word_tokenize(text))
    named_entities = nltk.ne_chunk(tagged_tokens, binary=False)

    # Extract person names and associated contribution types from the named entities
    for entity in named_entities.subtrees():
        if entity.label() == 'PERSON':
            person_name = ' '.join([leaf[0] for leaf in entity.leaves()])
            for regex in contribution_regexes:
                match = re.search(regex, text, re.IGNORECASE)
                if match:
                    contribution_type = match.group(0)
                    contributions.add((person_name, contribution_type))
                else:
                    contributions.add(person_name)

    # Return the list of contributions
    return list(contributions)


def marc_contributors(identifier):
    response = requests.get(f'http://bibliographics.live.bcommons.net/api/v1/bibs/{identifier}/marcFields')

    contributors = []
    if response.status_code == 200:
        # Convert the response content to JSON
        json_data = json.loads(response.content)

        # Loop through the JSON data and print the title of each post
        for field in json_data['marcFields']:
            for subfield in field['subFields']:
                if subfield['subTag'] == '0':
                    continue

                contributions = extract_contributions(subfield['value'])
                if contributions:
                    contributors.append(f'{field["tag"]}{subfield["subTag"]} {contributions}')
    else:
        print('Request failed with status code:', response.status_code)

    return contributors


if __name__ == '__main__':
    metadata_id = 'S209C3630468'

    # Check if the request was successful
    for contributor in marc_contributors(metadata_id):
        print(contributor)
