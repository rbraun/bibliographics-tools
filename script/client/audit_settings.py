import requests
import csv
import time
import enum


class Column(enum.Enum):
    CLIENT_ID = "client_id"
    CLIENT_NAME = "client_name"
    RETIRED = "retired"
    NOVELIST_PROFILE = "novelist_profile"
    SYNDETICS_KEY = "syndetics_key"
    CONTENTCAFE_KEY = "contentcafe_key"
    MMM = "mmm"
    HOOPLA = "HooplaAPI"
    OVERDRIVE = "OverDriveAPI"
    SCOPED = "scoped"


if __name__ == "__main__":
    with open('client_settings.csv', 'w', newline='') as csvfile:

        writer = csv.DictWriter(csvfile, fieldnames=[column.value for column in Column])

        writer.writeheader()
        for client_id in range(1, 220):
            response = requests.get(
                f"http://core-liv-nerf01.bcommons.net:8082/v3/assets/settings?libraryId={client_id}").json()

            if "fieldErrors" in response:
                continue

            data = {Column.CLIENT_ID.value: client_id}

            if 'novelist_parameters' in response:
                novelist_parameters = response['novelist_parameters']
                if novelist_parameters and 'profile' in novelist_parameters:
                    novelist_profile = novelist_parameters['profile']
                    if novelist_profile and not novelist_profile.isspace():
                        try:
                            novelist_response = requests.get(f"https://novselect.ebscohost.com/Data/ContentByQuery?profile={novelist_profile}&password={novelist_parameters['password']}&ClientIdentifier=9781626721418&ISBN=9781626721418")
                            warn = "!!!" if novelist_response.status_code == 403 else ""
                        except Exception as e:
                            warn = "!!!"

                        data[Column.NOVELIST_PROFILE.value] = warn + novelist_profile + warn

            if "library_full_name" in response:
                names = response["library_full_name"]
                data[Column.CLIENT_NAME.value] = names[0] if type(names) is list else names

            if 'syndetics_parameters' in response:
                syndetics_parameters = response['syndetics_parameters']
                if syndetics_parameters and 'syndetics_client_key' in syndetics_parameters:
                    syndetics_client_key = syndetics_parameters['syndetics_client_key']
                    if syndetics_client_key and not syndetics_client_key.isspace():
                        data[Column.SYNDETICS_KEY.value] = syndetics_client_key

            if 'contentcafe_parameters' in response:
                contentcafe_parameters = response['contentcafe_parameters']
                if contentcafe_parameters and 'contentcafe_client_key' in contentcafe_parameters:
                    contentcafe_client_key = contentcafe_parameters['contentcafe_client_key']
                    if contentcafe_client_key and not contentcafe_client_key.isspace():
                        data[Column.CONTENTCAFE_KEY.value] = contentcafe_client_key

            if "closed_to_public" in response:
                closed_to_public = response["closed_to_public"]
                if closed_to_public:
                    data[Column.RETIRED.value] = "yes"

            if "digital_service_type" in response:
                digital_service_type = response["digital_service_type"]
                if "MMM" in digital_service_type or "mmm" in digital_service_type:
                    data[Column.MMM.value] = "yes"
                if "HooplaAPI" in digital_service_type or "hooplaapi" in digital_service_type:
                    data[Column.HOOPLA.value] = "yes"
                if "OverDriveAPI" in digital_service_type or "overdriveapi" in digital_service_type:
                    data[Column.OVERDRIVE.value] = "yes"

            if "scoped" in response:
                scoped = response["scoped"]
                if scoped:
                    data[Column.SCOPED.value] = "yes"

            writer.writerow(data)
            time.sleep(1)
