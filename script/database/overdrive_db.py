from client import database
import psycopg2

from client.database import postgres
from client.database.postgres import OverDriveDatabase, LibraryDatabase, MarcDatabase
from environment import Environment
from logger import logging
import pprint
import json


def product_details(product_id):
    overdrive = OverDriveDatabase(Environment.STAGE)
    od_cursor = overdrive.db.cursor()

    od_cursor.execute(f"select id, updated_date, raw_json from overdrive_product where product_id = '{product_id}'")
    record = od_cursor.fetchone()

    id, updated_date, raw_json = str(record[0]), str(record[1]), json.dumps(json.loads(record[2]), indent=2)
    logging.info(f"S980C{id} was last updated at {updated_date}.")
    return raw_json


def find_product_with_media_type(media_type):
    overdrive = OverDriveDatabase(Environment.STAGE)
    od_cursor = overdrive.db.cursor()
    od_cursor.itersize = 1000
    od_cursor.execute(f"select product_id, raw_json from overdrive_product")

    while True:
        rows = od_cursor.fetchmany(od_cursor.itersize)
        if not rows:
            break

        for row in rows:
            metadata = json.loads(row[1])
            if "mediaType" in metadata:
                print(metadata['mediaType'])
            if "mediaType" in metadata and metadata['mediaType'] == media_type:
                return row[0]


def audit_overdrive_product(product_id):
    logging.info(f"Auditing libraries for overdrive product {product_id}.")

    overdrive = OverDriveDatabase(Environment.STAGE)
    od_cursor = overdrive.db.cursor()

    od_cursor.execute(f"select site_id from overdrive_metadata_sync_control")
    od_libs = list(map(lambda x: str(x[0]), od_cursor.fetchall()))

    logging.info(f"Searching {len(od_libs)} libraries with active overdrive syncs...")

    owners = {}
    for lib in od_libs:
        lib_config = postgres.get_client_config(Environment.STAGE, lib)
        # host, port, database, lib_id
        library = MarcDatabase(Environment.STAGE, lib_config["host"], lib_config["port"], lib_config["dbname"], lib_config["id"])
        lib_cursor = library.db.cursor()
        lib_cursor.execute(f"select holdings_collection, updated_date from digital_sync_holdings where "
                           f"holdings_origin = 'OVERDRIVE' and lower(holdings_id) = lower('{product_id}')")
        results = list(map(lambda x: f"{str(x[0])}: {str(x[1])}", lib_cursor.fetchall()))
        if len(results) > 0:
            logging.info(f"{lib} had product updated at {results}.")
            owners[lib_config['agency']] = results
        else:
            print(f"{lib} does not own product.")

        lib_cursor = library.db.cursor()
        lib_cursor.execute(
            f"select count(distinct holdings_id) from digital_sync_holdings where holdings_origin = 'OVERDRIVE' and "
            f"updated_date < '2020-05-15 18:57:10.781'")
        old = str(lib_cursor.fetchone()[0])

        lib_cursor = library.db.cursor()
        lib_cursor.execute(
            f"select count(distinct holdings_id) from digital_sync_holdings where holdings_origin = 'OVERDRIVE'")
        logging.info(f"{lib}: {old} / {str(lib_cursor.fetchone()[0])} overdrive holdings last updated before May 15.")

    logging.info("Finished overdrive product audit.")

    return owners


if __name__ == "__main__":
    product = "47952de3-a234-40de-aa39-0cd5c8f50584"
    pprint.pprint(audit_overdrive_product(product))

    product = 'F37D7501-5C37-4A0D-9572-762CA9BC792D'
    print(product_details(product))
