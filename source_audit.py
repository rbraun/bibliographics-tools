from client.database import postgres
from client.database.postgres import MarcDatabase
from environment import Environment
from logger import logging
import itertools
import client_aws
from client import database
import numpy as np
import matplotlib.pyplot as plt

auth_only = False
aws = client_aws.AwsResources('live')


def sample(objects: set, count: int = 10000):
    return set(itertools.islice(objects, count))


def audit_source(client):
    logging.info("*****************************************************************************************")

    config = postgres.get_client_config(Environment.LIVE, client)

    logging.info(f"Auditing {config['agency']}.")
    logging.info(f"Configuration = {config}")

    library = MarcDatabase(Environment.LIVE, config["host"], config["port"], config["dbname"], config["id"])

    logging.info(f"Loading metadata ids from DB...")

    cursor = library.db.cursor()
    cursor.execute(config["query"])
    db_ids = set(map(lambda x: f"S{config['id']}C" + str(x[0]), cursor.fetchall()))

    logging.info(f"{library.db.dsn} = {len(db_ids)}")

    sources = [
        client_aws.MetadataSource.CONTENT_CAFE,
        client_aws.MetadataSource.SYNDETICS,
        client_aws.MetadataSource.I_DREAM_BOOKS
    ]

    for k,v in client_aws.audit_dynamodb_sources(aws, db_ids, sources).items():
        logging.info(f"{k} = {v}")

    logging.info(f"Finished audit of {client}!")


def audit_excerpt_length(client):
    logging.info("*****************************************************************************************")

    config = postgres.get_client_config(Environment.LIVE, client)

    logging.info(f"Auditing {config['agency']}.")
    logging.info(f"Configuration = {config}")

    library = MarcDatabase(Environment.LIVE, config["host"], config["port"], config["dbname"], config["id"])

    logging.info(f"Loading metadata ids from DB...")

    cursor = library.db.cursor()
    cursor.execute(config["query"])
    db_ids = set(map(lambda x: f"S{config['id']}C" + str(x[0]), cursor.fetchall()))

    logging.info(f"{library.db.dsn} = {len(db_ids)}")

    sources = [
        client_aws.MetadataSource.SYNDETICS
    ]

    results = client_aws.audit_dynamodb_excerpt_lengths(aws, db_ids, sources)

    n, bins, patches = plt.hist(x=results, bins='auto', color='#0504aa',
                                alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.xlabel('Length')
    plt.ylabel('Frequency')
    plt.title(f'Excerpt Lengths (N={len(results)})')
    maxfreq = n.max()
    # Set a clean upper y-axis limit.
    plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)

    logging.info(f'mean={np.mean(results)}, median={np.median(results)}, percentiles={np.percentile(results, [50, 75, 85, 90, 95, 99, 99.9])}')
    plt.show()

    logging.info(f"Finished audit of {client}!")


def audit_lifecycles(client):
    logging.info("*****************************************************************************************")

    config = postgres.get_client_config(Environment.LIVE, client)

    logging.info(f"Auditing {config['agency']}.")
    logging.info(f"Configuration = {config}")

    connection = MarcDatabase(Environment.LIVE, config["host"], config["port"], config["dbname"], config['id'])

    logging.info(f"Loading metadata ids from DB...")

    cursor = connection.db.cursor()
    cursor.execute(config["query"])
    db_ids = set(map(lambda x: f"S{config['id']}C" + str(x[0]), cursor.fetchall()))

    logging.info(f"{connection.db.dsn} = {len(db_ids)}")

    sources = [
        client_aws.MetadataSource.CONTENT_CAFE
    ]

    results = client_aws.audit_dynamodb_lifecycles(aws, db_ids, sources)

    logging.info(f"{len(results)} STALE records found!")
    logging.info(f"Finished audit of {client}!")


if __name__ == "__main__":
    #clients = sys.argv[1].split(',')
    clients = ['CAL-SANDIEGOCOUNTY']
    for client in clients:
        audit_source(client)
        #audit_lifecycles(client)
