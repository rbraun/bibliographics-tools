import logging

logging.basicConfig(filename='audit_client.log', format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))
logging.getLogger().addHandler(handler)
