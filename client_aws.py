import boto3
import enum
from logger import logging
import itertools
import json
import requests


class AwsResources:
    def __init__(self, env, dynamodb_region: str = 'ca-central-1', lambda_region: str = 'ca-central-1'):
        self.env = env

        self.session = boto3.Session(profile_name=env)
        self.s3 = self.session.resource('s3')
        self.dynamodb = self.session.resource('dynamodb', region_name=dynamodb_region)
        self.lambdas = self.session.client('lambda', region_name=lambda_region)
        self.wildcard_bucket_pattern = f"bibservice-*-marc-records-{self.env}"

    def client_bucket(self, client_id: int):
        return self.s3.Bucket(f"bibservice-{client_id}-marc-records-{self.env}")

    def bibliographic_records_table(self):
        return f'{self.env}_bibliographic_records'

    def dynamodb_get_items(self, table_name, metadataids):
        return self.dynamodb.meta.client.batch_get_item(
            RequestItems={table_name: {
                'Keys': metadataids,
                'ProjectionExpression': 'metadataId,#s',
                'ExpressionAttributeNames': {'#s': 'source'}
        }})

    def dynamodb_get_excerpts(self, table_name, metadataids):
        return self.dynamodb.meta.client.batch_get_item(
            RequestItems={table_name: {
                'Keys': metadataids,
                'ProjectionExpression': 'metadataId,#s,excerpts',
                'ExpressionAttributeNames': {'#s': 'source'}
        }})

    def dynamodb_get_lifecycle(self, table_name, metadataids):
        return self.dynamodb.meta.client.batch_get_item(
            RequestItems={table_name: {
                'Keys': metadataids,
                'ProjectionExpression': 'metadataId,#s,lifeycleState',
                'ExpressionAttributeNames': {'#s': 'source'}
        }})

    def permitted_client_sids(self, lambda_name):
        clients = {}
        statements = json.loads(self.lambdas.get_policy(FunctionName=lambda_name)['Policy'])['Statement']
        for item in statements:
            try:
                clients[item['Sid']] = item['Condition']['ArnLike']['AWS:SourceArn']
            except KeyError:
                clients[item['Sid']] = None

        return clients

    def remove_permission(self, lambda_name, sid):
        self.lambdas.remove_permission(FunctionName=lambda_name, StatementId=sid)


class S3Folder(enum.Enum):
    AUTH_SYNC = 'auth_sync'
    BIC_CORRECTIONS = 'bib_corrections'
    BIB_SYNC = 'bib_sync'
    CONTENTCAFE_SYNC = 'contentcafe_sync'
    IDREAMBOOKS_SYNC = 'idreambooks_sync'
    PROCESSED_AUTHS = 'processed_auths'
    PROCESSED_BIBS = 'processed_bibs'
    RAW_AUTHS = 'raw_auths'
    RAW_BIBS = 'raw_bibs'
    SYNDETICS_SYNC = 'syndetics_sync'


class MetadataSource(enum.Enum):
    MARC = 'MARC'
    OVERDRIVE = 'OVERDRIVE'
    HOOPLA = 'HOOPLA'
    ADDENDUM_1 = 'ADDENDUM_1'
    SYNDETICS = 'SYNDETICS'
    CONTENT_CAFE = 'CONTENT_CAFE'
    I_DREAM_BOOKS = 'I_DREAM_BOOKS'
    GROUP_KEY = 'GROUP_KEY'
    GROUP_KEY_LEGACY = 'GROUP_KEY_LEGACY'
    ADMIN = 'ADMIN'
    MARC_BIB_HOLDINGS = 'MARC_BIB_HOLDINGS'
    OVERDRIVE_BIB_HOLDINGS = 'OVERDRIVE_BIB_HOLDINGS'
    HOOPLA_BIB_HOLDINGS = 'HOOPLA_BIB_HOLDINGS'
    USER_ADDED = 'USER_ADDED'
    FALLBACK = 'FALLBACK'


def sample(objects: set, count: int = 3500):
    return set(itertools.islice(objects, count))


def audit_folder(resources: AwsResources, client_id: int, folder: S3Folder, expected_ids: set):
    s3_ids = __load_folder_ids(resources, client_id, folder)

    missing_expected_ids = expected_ids.difference(s3_ids)
    unknown_s3_ids = s3_ids.difference(expected_ids)

    if len(missing_expected_ids) > 0:
        logging.error(
            f"{len(missing_expected_ids)} expected ids missing from S3 {folder.value}: {sample(missing_expected_ids)}")
    if len(unknown_s3_ids) > 0:
        logging.error(f"{len(unknown_s3_ids)} unexpected ids found on S3 {folder.value}: {sample(unknown_s3_ids)}")


def __load_folder_ids(resources: AwsResources, client_id: int, folder: S3Folder):
    bucket = resources.client_bucket(client_id)
    logging.log(logging.INFO, f"Loading ids from S3 {folder.value} bucket...")

    s3_ids = set(map(lambda obj: obj.key[len(folder.value) + 1:], bucket.objects.filter(Prefix=folder.value)))
    if '' in s3_ids:
        s3_ids.remove('')  # the folder itself may be included in filtered objects

    logging.log(logging.INFO, f"Loaded {len(s3_ids)} ids from S3 {folder.value} bucket.")

    return s3_ids


def audit_dynamodb(resources: AwsResources, metadata_ids, source):
    logging.info(f"Verifying {len(metadata_ids)} metadata ids are present in dynamodb...")
    metadata_ids_to_verify = set(metadata_ids)

    dynamodb_ids = set()
    while len(metadata_ids_to_verify) > 0:
        last_length = len(metadata_ids_to_verify)
        keys = []
        for metadata_id in sample(metadata_ids_to_verify, 100):
            metadata_ids_to_verify.remove(metadata_id)
            keys.append({'metadataId': metadata_id, 'source': source})

        table_name = resources.bibliographic_records_table()
        response = resources.dynamodb_get_items(table_name, keys)

        dynamodb_ids.update(map(lambda item: item['metadataId'], response['Responses'][table_name]))
        if len(response['UnprocessedKeys']) > 0:
            metadata_ids_to_verify.update(
                map(lambda item: item['metadataId'], response['UnprocessedKeys'][table_name]['Keys']))
            print("Encountered unprocessed keys!")

        if len(metadata_ids_to_verify) >= last_length:
            logging.error("No keys were processed in a while.")
            exit(-1)

    return dynamodb_ids


def audit_dynamodb_sources(resources: AwsResources, metadata_ids, sources):
    source_counts = {}
    for source in sources:
        source_counts[source.value] = 0

    logging.info(f"Counting sources for {len(metadata_ids)} metadata ids in dynamodb...")
    metadata_ids_to_verify = set(metadata_ids)

    table_name = resources.bibliographic_records_table()

    index = 0
    while len(metadata_ids_to_verify) > 0:
        keys = []
        for metadata_id in sample(metadata_ids_to_verify, int(100 / len(sources))):
            metadata_ids_to_verify.remove(metadata_id)
            for source in sources:
                keys.append({'metadataId': metadata_id, 'source': source.value})

        response = resources.dynamodb_get_items(table_name, keys)

        for source in map(lambda item: item['source'], response['Responses'][table_name]):
            source_counts[source] = source_counts[source] + 1

        index += 1
        if index % 1000 == 0:
            print(len(metadata_ids_to_verify))

    return source_counts


def audit_dynamodb_excerpt_lengths(resources: AwsResources, metadata_ids, sources):
    logging.info(f"Determining excerpt lengths for {len(metadata_ids)} metadata ids in dynamodb...")
    metadata_ids_to_verify = set(metadata_ids)

    table_name = resources.bibliographic_records_table()

    results = []

    index = 0
    while len(metadata_ids_to_verify) > 0:
        keys = []
        for metadata_id in sample(metadata_ids_to_verify, int(100 / len(sources))):
            metadata_ids_to_verify.remove(metadata_id)
            for source in sources:
                keys.append({'metadataId': metadata_id, 'source': source.value})

        response = resources.dynamodb_get_excerpts(table_name, keys)

        for excerpts in map(lambda item: item['excerpts'], response['Responses'][table_name]):
            for excerpt in excerpts:
                results.append(len(excerpt['excerpt']))

        index += 1
        if index % 1000 == 0:
            print(len(metadata_ids_to_verify))

    return results


def audit_dynamodb_lifecycles(resources: AwsResources, metadata_ids, sources):
    logging.info(f"Determining lifecycles for {len(metadata_ids)} metadata ids in dynamodb...")
    metadata_ids_to_verify = set(metadata_ids)

    table_name = resources.bibliographic_records_table()

    results = []

    index = 0
    while len(metadata_ids_to_verify) > 0:
        keys = []
        for metadata_id in sample(metadata_ids_to_verify, int(100 / len(sources))):
            metadata_ids_to_verify.remove(metadata_id)
            for source in sources:
                keys.append({'metadataId': metadata_id, 'source': source.value})

        response = resources.dynamodb_get_lifecycle(table_name, keys)

        for metadata_id in map(lambda item: item['metadataId'] if item['lifeycleState'] == "STALE" else None, response['Responses'][table_name]):
            if metadata_id:
                results.append(metadata_id)
                url = f"http://bibliographics001.core.prod.bf1.corp.pvt:8090/api/v1/catalog/bibs/{metadata_id}?subdomain=sdcl"
                requests.get(url)
                logging.info(metadata_id)

        index += 1
        if index % 1000 == 0:
            print(len(metadata_ids_to_verify))

    return results


if __name__ == "__main__":
    aws = AwsResources('live')

    db_ids = ['S182C337301', 'S30C3403140', 'S30C3407401', 'S30C2073594', 'S30C3464074', 'S30C3499093', 'S30C3290221',
              'S30C2342944', 'S30C2944370', 'S30C3403293', 'S30C3434613', 'S30C1297492', 'S30C3498790', 'S30C3433274',
              'S30C3403745', 'S30C3408156', 'S30C3408567', 'S30C3403102', 'S30C3436174', 'S30C3432812']
    for item in audit_dynamodb_lifecycles(aws, db_ids, [MetadataSource.CONTENT_CAFE]):
      print(f"{item}")

    # dynamodb_ids = set(audit_dynamodb(aws, db_ids, MetadataSource.CONTENT_CAFE.value))
    #missing_dynamodb_ids = set(db_ids).difference(dynamodb_ids)
    #if len(missing_dynamodb_ids) > 0:
    #    logging.error(
    #        f"{len(missing_dynamodb_ids)} metadata ids from DB missing from dynamodb: {sample(missing_dynamodb_ids)}")

    # client = "LA-STTAMMANY"
    # for k,v in audit_dynamodb_sources(aws, ["S129C416234"], [MetadataSource.MARC, MetadataSource.CONTENT_CAFE]).items():
    #     print(f"{k} = {v}")

    # config = client_db.get_client_config(client)
    # __load_folder_ids(aws, config['id'], S3Folder.RAW_AUTHS)

    # policy read permissions need to be opened before this will work on prod
    # logging.info(aws.permitted_client_sids('BibSyncHorizon'))
