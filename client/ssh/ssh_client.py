from dotenv import dotenv_values
from environment import Environment
import paramiko
from paramiko.ssh_exception import AuthenticationException


class Ssh:
    def __init__(self, hostname):
        config = dotenv_values("client/ssh/.env")

        self.biblio_password = config.get("biblio_password")
        self.keys_location = config.get("keys_location")

        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._connect(hostname)

    def _connect(self, hostname):
        try:
            if hostname == 'slr-liv-smka-jsv02.bcommons.net':
                self.ssh.connect(hostname, username='biblio', password=self.biblio_password)
            else:
                self.ssh.connect(hostname, username='ansible',
                                 key_filename=f'{self.keys_location}/ansible/keys/integration_ansible.pem')
        except OSError as e:
            print(f"ERROR: Couldn't authenticate ssh to {hostname}: {e}.")
            self.ssh = None
            return None
        except AuthenticationException as e:
            print(f"ERROR: Couldn't authenticate ssh to {hostname}: {e}.")
            self.ssh = None
            return None

    def execute_command(self, command):
        if self.ssh:
            # execute command on remote host
            stdin, stdout, stderr = self.ssh.exec_command(command)
            stdin.close()  # paramiko bug workaround

            # read the output of the command
            return stdout.read(), stderr.read()
        else:
            print("ERROR: ssh session was not initialized correctly.")
            return None, "ERROR: ssh session was not initialized correctly."

    def close(self):
        if self.ssh:
            self.ssh.close()
