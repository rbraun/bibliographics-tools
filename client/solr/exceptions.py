class ReplicaAlreadyExists(Exception):
    pass


class ReplicaNotFound(Exception):
    pass


class CollectionAlreadyExists(Exception):
    pass


class AliasAlreadyExists(Exception):
    pass


class CollectionIsMissing(Exception):
    pass
