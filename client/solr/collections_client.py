import time
import urllib.request

from client.solr.exceptions import ReplicaAlreadyExists, ReplicaNotFound, CollectionAlreadyExists, AliasAlreadyExists, \
    CollectionIsMissing
from client.solr.status_client import ClusterStatusClient, Replica


class CollectionsClient:
    COLLECTION_BASE_ENDPOINT = 'http://{server}:8983/solr/admin/collections'
    ADD_REPLICA_ACTION = COLLECTION_BASE_ENDPOINT + \
        '?action=ADDREPLICA&collection={collection}&shard=shard1&node={server}:8983_solr'
    REMOVE_REPLICA_ACTION = COLLECTION_BASE_ENDPOINT + \
        '?action=DELETEREPLICA&collection={collection}&shard=shard1&replica={replica}'
    CREATE_COLLECTION = COLLECTION_BASE_ENDPOINT + \
        '?action=CREATE&name={collection}&numShards=1&replicationFactor=2&maxShardsPerNode=1&createNodeSet={nodeset}' \
        '&collection.configName=library&property.config=solrconfig.xml&property.schema=schema.xml'
    CREATE_ALIAS = COLLECTION_BASE_ENDPOINT + \
        '?action=CREATEALIAS&name={alias}&collections={collection}'

    def __init__(self, server):
        self.server = server
        self.status_client = ClusterStatusClient(server)

    def add_replica(self, collection):
        status = self.status_client.get_current_status()
        if status.get_replica_for_collection_on_server(collection, self.server):
            raise ReplicaAlreadyExists

        url = self.ADD_REPLICA_ACTION.format_map({'server': self.server, 'collection': collection})

        print(f'Creating replica for {collection} on {self.server}.')
        print(urllib.request.urlopen(url).read())

        print('Waiting for replica to become active:')
        while True:
            time.sleep(30)
            status = self.status_client.get_current_status()
            replica = status.get_replica_for_collection_on_server(collection, self.server)
            print(f'...{replica.state}')
            if replica.state == Replica.State.ACTIVE:
                break
        print('Done!')

    def remove_replica(self, collection):
        status = self.status_client.get_current_status()
        replica = status.get_replica_for_collection_on_server(collection, self.server)
        if not replica:
            raise ReplicaNotFound

        url = self.REMOVE_REPLICA_ACTION.format_map({'server': self.server, 'collection': collection,
                                                     'replica': replica.name})

        print(f'Removing replica for {collection} on {self.server}.')
        print(urllib.request.urlopen(url).read())

        print('Checking that the replica is gone:')
        time.sleep(2)
        while True:
            status = self.status_client.get_current_status()
            replica = status.get_replica_for_collection_on_server(collection, self.server)
            if not replica:
                break
            print(f'...{replica.state}')
            time.sleep(30)
        print('Gone!')

    def create_collection(self, collection, nodeset):
        status = self.status_client.get_current_status()
        replicas = status.get_replicas_for_collection(collection)
        if any(replicas):
            raise CollectionAlreadyExists

        url = self.CREATE_COLLECTION.format_map({'server': self.server, 'collection': collection, 'nodeset': nodeset})

        print(f'Creating {collection} on nodeset {nodeset}.')
        print(urllib.request.urlopen(url).read())

        print(f'Checking that {collection} replicas exist:')
        time.sleep(5)
        while True:
            status = self.status_client.get_current_status()
            replicas = status.get_replicas_for_collection(collection)
            if any(replicas):
                break
            print(f'...none found yet')
            time.sleep(30)

        replicas = status.get_replicas_for_collection(collection)
        print(f'Found replicas on {", ".join(replica.server for replica in replicas)}.')

    def create_alias(self, alias, collection):
        status = self.status_client.get_current_status()
        aliases = status.get_aliases()
        if alias in aliases:
            raise AliasAlreadyExists

        if collection not in status.get_all_collections():
            raise CollectionIsMissing

        url = self.CREATE_ALIAS.format_map({'server': self.server, 'alias': alias, 'collection': collection})

        print(f'Creating alias {alias} to {collection}.')
        print(urllib.request.urlopen(url).read())
        time.sleep(1)


if __name__ == '__main__':
    SERVER_HOST = 'solr-liv-cld21.bcommons.net'
    client = CollectionsClient(SERVER_HOST)

    client.create_alias('UGC-SHELF-CAN-OAK-HORIZON', 'DEV_NULL')
