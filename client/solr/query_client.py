import urllib.request
import urllib.parse
import json


class QueryClient:
    QUERY = 'http://{server}:8983/solr/{collection}/select?indent=on&q={query}&wt=json'

    def __init__(self, server):
        self.server = server

    def get_result_count(self, collection, query):
        url = self.QUERY.format_map({"server": self.server, "collection": collection,
                                     "query": urllib.parse.quote(query)})
        result = json.loads(urllib.request.urlopen(url).read())
        return int(result['response']['numFound'])
