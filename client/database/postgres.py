import sys
import psycopg2
import json
import yaml
import pprint
import os

from dotenv import dotenv_values
from abc import ABC, abstractmethod

from environment import Environment

DB_CONFIGS_DIR = "client/database/config"

DB_PASSWORD = 'db.password'
APP_HOST = 'db.app.host'
APP_PORT = 'db.app.port'
APP_NAME = 'db.app.name'
DIGITAL_HOST = "db.digital.host"
DIGITAL_PORT = "db.digital.port"


class PostgresDatabase(ABC):
    def __init__(self, environment):
        self.config = {
            **environment.config,
            **dotenv_values(f"client/database/.env.{environment.value}"),
            **dotenv_values("client/database/.env")
        }

        self.db = psycopg2.connect(user="postgres", password=self.config[DB_PASSWORD], host=self.get_host(),
                                   port=self.get_port(), database=self.get_database())

    @abstractmethod
    def get_host(self):
        pass

    @abstractmethod
    def get_port(self):
        pass

    @abstractmethod
    def get_database(self):
        pass


class ApplicationDatabase(PostgresDatabase):
    def get_host(self):
        return self.config[APP_HOST]

    def get_port(self):
        return self.config[APP_PORT]

    def get_database(self):
        return self.config[APP_NAME]

    def __init__(self, environment):
        super().__init__(environment)

    def get_control_settings(self, agency_code):
        cursor = self.db.cursor()
        cursor.execute(f"SELECT asset_data FROM library_assets WHERE archive = false AND asset_cd = 'CONTROL' AND "
                       f"agency_id = '{agency_code}'")
        result = cursor.fetchone()
        if not result:
            raise Exception(f"Could not load control assets for {agency_code}.")
        return yaml.safe_load(result[0].tobytes())

    def get_cluster_db_host_and_port(self, cluster_name):
        cursor = self.db.cursor()
        cursor.execute(f"SELECT database_url_base_readonly FROM library_cluster WHERE cluster_name = "
                       f"'{cluster_name}'")
        return str(cursor.fetchone()[0])[len("jdbc:postgresql://"):].strip('/').split(':', 1)

    def get_agency_code(self, source_id):
        cursor = self.db.cursor()
        cursor.execute(f"SELECT agency_id FROM library WHERE lib_id = {source_id}")
        result = cursor.fetchone()
        if not result:
            raise Exception(f"Could not determine agency code for {source_id}.")
        return str(result[0])

    def get_source_id(self, agency_code):
        cursor = self.db.cursor()
        cursor.execute(f"SELECT lib_id FROM library WHERE agency_id = '{agency_code}'")
        result = cursor.fetchone()
        if not result:
            raise Exception(f"Could not determine lib id for {agency_code}.")
        return str(result[0])


class LibraryDatabase(PostgresDatabase):
    def __init__(self, environment, client_id):
        self.id = client_id

        super().__init__(environment)

    @abstractmethod
    def get_authority_count(self):
        pass

    @abstractmethod
    def get_bib_count(self):
        pass

    @abstractmethod
    def get_bib_id_query(self):
        pass

    @abstractmethod
    def get_type(self):
        pass


class MarcDatabase(LibraryDatabase):
    def __init__(self, environment, host, port, database, client_id):
        self.host = host
        self.port = port
        self.database = database

        super().__init__(environment, client_id)

    def get_host(self):
        return self.host

    def get_port(self):
        return self.port

    def get_database(self):
        return self.database

    def get_authority_count(self):
        cursor = self.db.cursor()
        cursor.execute(f"select count(*) from marc_library_auth where lib_id = {self.id}")
        return int(cursor.fetchone()[0])

    def get_bib_count(self):
        cursor = self.db.cursor()
        cursor.execute(f"select count(*) from library_bib where lib_id = {self.id}")
        return int(cursor.fetchone()[0])

    def get_bib_id_query(self):
        return f"select library_bib_id from library_bib where lib_id = {self.id}"

    def get_type(self):
        return 'MARC'


class OverDriveDatabase(LibraryDatabase):
    def __init__(self, environment):
        super().__init__(environment, 980)

    def get_authority_count(self):
        return 0

    def get_bib_count(self):
        cursor = self.db.cursor()
        cursor.execute(f"select count(*) from overdrive_product")
        return int(cursor.fetchone()[0])

    def get_bib_id_query(self):
        return "select id from overdrive_product"

    def get_type(self):
        return 'OVERDRIVE'

    def get_host(self):
        return self.config[DIGITAL_HOST]

    def get_port(self):
        return self.config[DIGITAL_PORT]

    def get_database(self):
        return "overdrive_metadata"


class HooplaDatabase(LibraryDatabase):
    def __init__(self, environment):
        super().__init__(environment, 981)

    def get_authority_count(self):
        return 0

    def get_bib_count(self):
        cursor = self.db.cursor()
        cursor.execute(f"select count(*) from hoopla_title_catalog")
        return int(cursor.fetchone()[0])

    def get_bib_id_query(self):
        return "select title_id from hoopla_title_catalog"

    def get_type(self):
        return 'HOOPLA'

    def get_host(self):
        return self.config[DIGITAL_HOST]

    def get_port(self):
        return self.config[DIGITAL_PORT]

    def get_database(self):
        return "hoopla"


def get_client_config(environment, agency_code):
    params = {}

    if agency_code == 'HOOPLA':
        source_db = HooplaDatabase(environment)

    elif agency_code == 'OVERDRIVE':
        source_db = OverDriveDatabase(environment)

    else:
        app_db = ApplicationDatabase(environment)

        # this could be automated from settings but it is an edge case
        aliases = json.load(open(f'{DB_CONFIGS_DIR}/ALIASES.json'))
        alias = aliases[agency_code] if agency_code in aliases else None

        settings = app_db.get_control_settings(agency_code if alias is None else alias)

        lib_id = app_db.get_source_id(agency_code)
        core_id = settings['solr_core_id'] if 'solr_core_id' in settings else None

        cluster = settings['library_cluster']
        host, port = app_db.get_cluster_db_host_and_port(cluster)
        database = settings['database_name']

        source_db = MarcDatabase(environment, host, port, database, lib_id)

        params['cluster'] = cluster
        params['ils'] = settings['ils_type']
        params['ils_enabled'] = settings['jobservice_sync_enabled']
        params['correction_enabled'] = settings['jobservice_correction_enabled']
        params['retired'] = not bool(core_id)

    params['agency'] = agency_code
    params['type'] = source_db.get_type()
    params['id'] = source_db.id
    params['dbname'] = source_db.get_database()
    params['host'] = source_db.get_host()
    params['port'] = source_db.get_port()
    params['query'] = source_db.get_bib_id_query()
    params['authorities'] = source_db.get_authority_count() > 0
    params['size'] = source_db.get_bib_count()

    return params


if __name__ == "__main__":
    env = Environment.LIVE
    clients = ['NS-HALIFAX']
    for client in clients:
        config = get_client_config(env, client)
        pprint.pprint(config)
