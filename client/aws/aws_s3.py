import boto3
from dotenv import dotenv_values
import re
from environment import Environment

S3_ACCESS_KEY = 's3.access.key'
S3_ACCESS_SECRET = 's3.access.secret'
S3_REGION = 's3.region'


# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html


class S3Client:
    def __init__(self, environment):
        self.config = {
            **environment.config,
            **dotenv_values(f'client/aws/.env.{environment.value}'),
            **dotenv_values('client/aws/.env')
        }

        self.client = boto3.client('s3', aws_access_key_id=self.config[S3_ACCESS_KEY],
                                   aws_secret_access_key=self.config[S3_ACCESS_SECRET],
                                   region_name=self.config[S3_REGION])
        self.resource = boto3.resource('s3', aws_access_key_id=self.config[S3_ACCESS_KEY],
                                       aws_secret_access_key=self.config[S3_ACCESS_SECRET],
                                       region_name=self.config[S3_REGION])

    def get_bucket_names_by_regex(self, regex='.*'):
        return {bucket['Name'] for bucket in self.client.list_buckets()['Buckets'] if re.match(regex, bucket['Name'])}

    def delete_folder_contents(self, bucket_name, folder_name):
        paginator = self.client.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket_name, Prefix=folder_name)

        total = 0

        # paging and delete ops are capped at 1000 results
        ids = dict(Objects=[])
        for item in pages.search('Contents'):
            # make sure we found contents
            if item is None:
                continue

            ids['Objects'].append(dict(Key=item['Key']))
            # flush once aws limit reached
            if len(ids['Objects']) >= 1000:
                self.client.delete_objects(Bucket=bucket_name, Delete=ids)

                total += len(ids['Objects'])
                ids = dict(Objects=[])

        if len(ids['Objects']):
            self.client.delete_objects(Bucket=bucket_name, Delete=ids)
            total += len(ids['Objects'])

        return total


if __name__ == "__main__":
    s3 = S3Client(Environment.STAGE)

    print(s3.get_bucket_names_by_regex(r'bibservice-\d+-marc-records-stage'))
