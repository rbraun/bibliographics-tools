import boto3
from dotenv import dotenv_values

from environment import Environment

LAMBDA_ACCESS_KEY = 'lambda.access.key'
LAMBDA_ACCESS_SECRET = 'lambda.access.secret'
LAMBDA_REGION = 'lambda.region'


# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html#Lambda.Client.list_functions

class LambdaClient:
    def __init__(self, environment):
        self.config = {
            **environment.config,
            **dotenv_values(f"client/aws/.env.{environment.value}"),
            **dotenv_values("client/aws/.env")
        }

        self.client = boto3.client('lambda', aws_access_key_id=self.config[LAMBDA_ACCESS_KEY],
                                   aws_secret_access_key=self.config[LAMBDA_ACCESS_SECRET],
                                   region_name=self.config[LAMBDA_REGION])

    def list_functions(self, prefix):
        function_names = []

        marker = None
        more_to_load = True

        # list_functions is paginated at 50 per call
        while more_to_load:
            response = self.client.list_functions(Marker=marker) if marker else self.client.list_functions()
            marker = response['NextMarker'] if 'NextMarker' in response else None
            fns = response['Functions']
            if fns:
                function_names.extend(fn['FunctionName'] for fn in fns if fn['FunctionName'].startswith(prefix))
            more_to_load = bool(marker)

        return function_names

    def get_queue_arns_for_lambda(self, function_name):
        queue_arns = []

        marker = ""
        more_to_load = True

        # list_event_source_mappings is paginated at 100 per call
        while more_to_load:
            response = self.client.list_event_source_mappings(FunctionName=function_name, Marker=marker)
            marker = response['NextMarker'] if 'NextMarker' in response else ""
            mappings = response['EventSourceMappings']
            if mappings:
                queue_arns.extend(mapping['EventSourceArn'] for mapping in mappings)
            more_to_load = bool(marker)

        return queue_arns

    def get_lambda_sqs_trigger_uuid(self, function_name, sqs_arn):
        mappings = self.client.list_event_source_mappings(EventSourceArn=sqs_arn,
                                                          FunctionName=function_name)['EventSourceMappings']
        queue_uuids = list(mapping['UUID'] for mapping in mappings if mapping['EventSourceArn'] == sqs_arn)

        # duped arn is an error
        if len(queue_uuids) > 1:
            raise Exception(f"Found duped lambda trigger: {mappings}.")

        return queue_uuids[0] if queue_uuids else None

    def add_lambda_sqs_trigger(self, function_name, sqs_arn):
        self.client.create_event_source_mapping(EventSourceArn=sqs_arn, FunctionName=function_name,
                                                Enabled=True, BatchSize=10)
        if not self.get_lambda_sqs_trigger_uuid(function_name, sqs_arn):  # sanity validation
            raise Exception(f"{function_name} could not be attached to {sqs_arn}.")

    def delete_lambda_sqs_trigger(self, function_name, sqs_arn):
        uuid = self.get_lambda_sqs_trigger_uuid(function_name, sqs_arn)
        response = self.client.delete_event_source_mapping(UUID=uuid)
        if response['EventSourceArn'] != sqs_arn:
            raise Exception(f"{function_name} trigger deletion contained an sqs arn other than {sqs_arn}.")


if __name__ == "__main__":
    function = LambdaClient(Environment.STAGE)

    functions = function.list_functions("bibliographics-sync")
    for function_name in functions:
        print(function_name)
    print(f"Found {len(functions)} functions.")
