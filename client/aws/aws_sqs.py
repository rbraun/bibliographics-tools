import boto3
from dotenv import dotenv_values

from environment import Environment

SQS_ACCESS_KEY = 'sqs.access.key'
SQS_ACCESS_SECRET = 'sqs.access.secret'
SQS_REGION = 'sqs.region'

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sqs.html#service-resource

class SqsQueue:
    def __init__(self, aws_queue):
        self.name = ''
        self.arn = ''
        self.attributes = {}

        if aws_queue:
            arn = aws_queue.attributes['QueueArn']
            if arn:
                self.arn = arn
                self.name = arn.split(':')[-1]
                self.attributes = aws_queue.attributes

    def __str__(self):
        return self.name


class SqsClient:
    def __init__(self, environment):
        self.config = {
            **environment.config,
            **dotenv_values(f"client/aws/.env.{environment.value}"),
            **dotenv_values("client/aws/.env")
        }

        self.client = boto3.client('sqs', aws_access_key_id=self.config[SQS_ACCESS_KEY],
                                   aws_secret_access_key=self.config[SQS_ACCESS_SECRET],
                                   region_name=self.config[SQS_REGION])
        self.resource = boto3.resource('sqs', aws_access_key_id=self.config[SQS_ACCESS_KEY],
                                       aws_secret_access_key=self.config[SQS_ACCESS_SECRET],
                                       region_name=self.config[SQS_REGION])

    def list_queues(self):
        queues = self.client.list_queues()['QueueUrls']
        print(len(queues))
        print(queues)

    def get_sqs_queues_by_prefix(self, name_prefix):
        return list(filter(lambda q: q.name.startswith(name_prefix), map(SqsQueue, self.resource.queues.all())))


if __name__ == "__main__":
    sqs = SqsClient(Environment.STAGE)
    for queue in sqs.get_sqs_queues_by_prefix('bibliographics-sync-enhancement'):
        print(queue.attributes)

    coll = list(sqs.resource.queues.all())

    print(coll)