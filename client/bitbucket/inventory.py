import requests
import re
from enum import Enum
from dotenv import dotenv_values

from environment import Environment


class ServerType(Enum):
    def __str__(self):
        return self.value

    ALL = "all"
    SOLR_SERVERS = "solrservers"
    SOLR_INDEXERS = "jobservice-solrcloud"


class Inventory:
    GROUPED_BY_CHILDREN_REGEX = re.compile(r'\[(.+):children]')
    GROUPED_REGEX = re.compile(r'\[(.+)]')
    BITBUCKET_INVENTORY_URL = \
        "https://api.bitbucket.org/2.0/repositories/bibliocommons/bc-deploy-provision/src/master/ansible/inventory"

    def __init__(self, env: Environment = Environment.STAGE):
        config = dotenv_values("client/bitbucket/.env")
        self.data = self._load_inventory_from_bitbucket(env.value, config.get('bitbucket_login'),
                                                        config.get('bitbucket_password'))

    def hosts(self, server_type):
        if self.data:
            return self.data[str(server_type)]
        else:
            raise Exception("Could not load inventory, check your credentials.")

    def server_types(self):
        return self.data.keys()

    def _load_inventory_from_bitbucket(self, env, login, password):
        host_groups = {}

        r = requests.get(f'{self.BITBUCKET_INVENTORY_URL}/{env}', auth=(login, password))
        lines = r.text.split('\n')

        hosts = []
        grouping_by_children = False
        for line in lines:
            if line.strip().startswith('##'):
                continue

            match = self.GROUPED_BY_CHILDREN_REGEX.fullmatch(line.strip())
            if match:
                grouping_by_children = True
                hosts = []
                host_groups[match.group(1)] = hosts
                continue

            match = self.GROUPED_REGEX.fullmatch(line.strip())
            if match:
                grouping_by_children = False
                hosts = []
                host_groups[match.group(1)] = hosts
                continue

            parts = line.split()
            if not parts:
                continue

            if grouping_by_children:
                hosts.extend(host_groups.get(parts[0]))
            else:
                hosts.append(parts[0])

        host_groups["all"] = host_groups[env]
        return host_groups


if __name__ == "__main__":
    env = Environment(input(f"Environment {[e.value for e in Environment]}: "))
    inventory = Inventory(env)
    print("Known server types:", *sorted(inventory.server_types()), sep="\n  ")
    server_type = input(f"Server Type: ")
    print(*inventory.hosts(server_type), sep="\n")
