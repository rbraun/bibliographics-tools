import logger
import boto3
from botocore.exceptions import ClientError

s3 = boto3.client('s3')
client_id = '981'

folders = [
    'auth_sync',
    'bib_corrections',
    'bib_sync',
    'contentcafe_sync',
    'idreambooks_sync',
    'processed_auths',
    'processed_bibs',
    'raw_auths',
    'raw_bibs',
    'syndetics_sync'
]

def create_bucket(bucket_name, region='us-west-2'):
    """Create an S3 bucket in a specified region

    :param bucket_name: Bucket to create
    :param region: String region to create bucket in, e.g., 'us-west-2'
    :return: True if bucket created, else False
    """

    # Create bucket
    try:
        if region is None:
            s3_client = boto3.client('s3')
            s3_client.create_bucket(Bucket=bucket_name)
        else:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name,
                                    CreateBucketConfiguration=location)
    except ClientError as e:
        logger.error(e)
        return False
    return True


bucket_names = {bucket['Name'] for bucket in s3.list_buckets()['Buckets']}

bucket_name = f'bibservice-{client_id}-marc-records-stage'
if bucket_name not in bucket_names:
    create_bucket(bucket_name)

for folder in folders:
    s3.put_object(Bucket=bucket_name, Key=(folder + '/'))
