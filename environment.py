from enum import Enum
from dotenv import dotenv_values


class Environment(Enum):
    STAGE = "stage"
    LIVE = "live"

    def __init__(self, value):
        self.config = {
            **dotenv_values(".env"),  # load personal variables
            **dotenv_values(f".env.{value}"),  # load environment-specific variables
        }

    def __str__(self):
        return self.value
