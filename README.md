# README #

This is a very preliminary set of tooling scripts for use with core services.  This code is not yet production
ready and does not conform to our python coding standards.  You can nuke databases and solr configuration quite
easily with this.  Use at your own risk.

## Setup

1. Clone this repository.
1. Load repo in pycharm.
2. Set up a python 3 conda environment for this project as per the directions at 
   https://bibliocommons.atlassian.net/wiki/spaces/COT/pages/854884721/Python+Development+Guidelines#Installing-and-Using-Conda.




# TODO BEFORE RELEASE #
+ Create an output directory for script outputs.
+ Update readme with complete installation instructions.
